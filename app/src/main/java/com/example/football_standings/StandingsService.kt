package com.example.football_standings

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface StandingsService {
    @GET("v2/competitions/")
    fun getCurrentWeatherData2(): Call<StandingsResponse>
    @GET("v2/competitions/{user_id}/standings/")
    fun getCurrentWeatherData(@Path("user_id") value : String, @Header("X-Auth-Token") authHeader:String): Call<StandingsResponse>
}