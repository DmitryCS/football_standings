package com.example.football_standings

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_competition_standings.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.FileOutputStream

class ActivityStandings:AppCompatActivity() {
    private var ids : ArrayList<Int> = ArrayList()
    private var teams : ArrayList<String> = ArrayList()
    private var info : ArrayList<String> = ArrayList()
    private var photos : ArrayList<String> = ArrayList()
    private var standingsDescription: TextView? = null
    private var httpClientSt: OkHttpClient? = null

    private fun share(name: String) {
        //val file = File(this.externalCacheDir?.path + "/image.png")
        val shareText = name
//        FileOutputStream(file).apply {
//            //image?.compress(Bitmap.CompressFormat.PNG, 100, this)
//            flush()
//            close()
//        }
        val intent = Intent(Intent.ACTION_SEND)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_STREAM,  FileProvider.getUriForFile(this, "com.ezmade.fileprovider", file))
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        intent.type = "text/plain"
        startActivity(Intent.createChooser(intent, "Share results"))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_competition_standings)

        val id_record = intent.getStringExtra("id")
        tv_standings_description.text = id_record
//        for (i in  0..100){
//            teams.add("Liverpool FC".plus(i.toString()))
//            info.add("All info ".plus(i.toString()))
//            val myDrawable = resources.getDrawable(R.drawable.no_picture)
//            val anImage = (myDrawable as BitmapDrawable).bitmap
//            photos.add(anImage)
//        }
        rv_standings.layoutManager = LinearLayoutManager(this)
        rv_standings.adapter = StandingsAdapter(teams, info, photos, this, this)

        standingsDescription = tv_standings_description
        getCurrentData(id_record!!)
        (rv_standings.adapter as StandingsAdapter).notifyDataSetChanged()

        //button.setOnClickListener { getCurrentData(id_record!!) }

        b_send.setOnClickListener{
            share((tv_standings_description.text as String?)!!)
        }
    }


    internal fun getCurrentData(id_record : String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(BaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(StandingsService::class.java)
        val call = service.getCurrentWeatherData(id_record , ServiceToken)
        call.enqueue(object : Callback<StandingsResponse> {
            override fun onResponse(call: Call<StandingsResponse>, response: Response<StandingsResponse>) {
                if (response.code() == 200) {
                    val standingsResponse = response.body()!!

                    val stringBuilder = "Name: " + standingsResponse.comp.name.toString() + "\nlastUpdated: " + standingsResponse.comp.lastUpdated + "\nstartDate: " + standingsResponse.season.startDate + "\nendDate: " + standingsResponse.season.endDate + "\ncurrentMatchday: " + standingsResponse.season.currentMatchday
                    standingsDescription!!.text = stringBuilder

                    if(standingsResponse.standings.size > 0) {
                        for (i in 0 until standingsResponse.standings[0].table.size) {
                            val infoBuilder =
                                "Position: " + standingsResponse.standings[0].table[i].position.toString() + "\nGames played: " + standingsResponse.standings[0].table[i].playedGames.toString() + "\nwon: " + standingsResponse.standings[0].table[i].won.toString() + ", draw: " + standingsResponse.standings[0].table[i].draw.toString() + ", lost: " + standingsResponse.standings[0].table[i].lost.toString() + "\n Points: " + standingsResponse.standings[0].table[i].points.toString() + ", GoalsFor: " + standingsResponse.standings[0].table[i].goalsFor.toString() + ", GoalsAgainst: " + standingsResponse.standings[0].table[i].goalsAgainst.toString()
                            val photo_url = standingsResponse.standings[0].table[i].team.crestUrl.toString()
                            teams.add(standingsResponse.standings[0].table[i].team.name!!)
                            info.add(infoBuilder)
                            photos.add(photo_url)
                            (rv_standings.adapter as StandingsAdapter).notifyDataSetChanged()
                        }
                    }
                    else{
                        Toast.makeText(applicationContext, "Data is empty.", Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<StandingsResponse>, t: Throwable) {
                standingsDescription!!.text = t.message
            }
        })
    }

    companion object {
        var BaseUrl = "https://api.football-data.org/"
        var ServiceToken = "d6cea22e136043d59ff72d5da805b77c"
    }
}