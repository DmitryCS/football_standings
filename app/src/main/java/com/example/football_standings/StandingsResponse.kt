package com.example.football_standings

import com.google.gson.annotations.SerializedName

class StandingsResponse {
    @SerializedName("count")
    var count: Int = 0
    @SerializedName("name")
    var name: String? = null
    @SerializedName("competition")
    var comp= Competition()
    @SerializedName("season")
    var season = Season()
    @SerializedName("standings")
    var standings = ArrayList<Standings>()
}

class Competition {
    @SerializedName("name")
    var name: String? = null
    @SerializedName("lastUpdated")
    var lastUpdated: String? = null
}

class Season{
    @SerializedName("startDate")
    var startDate: String? = null
    @SerializedName("endDate")
    var endDate: String? = null
    @SerializedName("currentMatchday")
    var currentMatchday: String? = null
}

class Standings{
    @SerializedName("table")
    var table = ArrayList<Table>()
}

class Table{
    @SerializedName("position")
    var position: Int = 0
    @SerializedName("team")
    var team = Team()
    @SerializedName("playedGames")
    var playedGames: Int = 0
    @SerializedName("won")
    var won: Int = 0
    @SerializedName("draw")
    var draw: Int = 0
    @SerializedName("lost")
    var lost: Int = 0
    @SerializedName("points")
    var points: Int = 0
    @SerializedName("goalsFor")
    var goalsFor: Int = 0
    @SerializedName("goalsAgainst")
    var goalsAgainst: Int = 0
}

class Team{
    @SerializedName("name")
    var name: String? = null
    @SerializedName("crestUrl")
    var crestUrl: String? = null
}