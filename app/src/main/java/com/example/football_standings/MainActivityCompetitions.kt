package com.example.football_standings

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.pixplicity.sharp.Sharp
import com.pixplicity.sharp.SharpPicture
import kotlinx.android.synthetic.main.activity_main_competitions.*
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.InputStream
import kotlin.collections.ArrayList


class MainActivityCompetitions : AppCompatActivity() {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    private var client = OkHttpClient()
    private var httpClient: OkHttpClient? = null
    private var ids: ArrayList<Int> = ArrayList()
    private var countries: ArrayList<String> = ArrayList()
    private var tournaments: ArrayList<String> = ArrayList()
    private var photos: ArrayList<Drawable> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_competitions)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)


//        for (i in  0..100){
//            countries.add("England ".plus(i.toString()))
//            tournaments.add("Championship ".plus(i.toString()))
//            val myDrawable = resources.getDrawable(R.drawable.no_picture)
//            val anImage = (myDrawable as BitmapDrawable).bitmap
//            photos.add(anImage)
//        }

        rv_competitions.layoutManager = LinearLayoutManager(this)
        rv_competitions.adapter = CompetitionAdapter(ids, countries, tournaments, photos, this, mFirebaseAnalytics!!)

        var responseStr: String? = null
        val requestJSON = Request.Builder()
            .url("https://api.football-data.org/v2/competitions") //2021/standings
            .header("X-Auth-Token", "d6cea22e136043d59ff72d5da805b77c")
            //.url("https://api.vk.com/method/friends.get?user_id=111&v=5.95&access_token=fd0a7792fd0a7792fd0a779278fd607b06ffd0afd0a7792a1bdfcd4e63cebb7063c23fc")
            .build()
        client.newCall(requestJSON).enqueue(object : Callback {
            override fun onFailure(call: Call, e: java.io.IOException) {}
            override fun onResponse(call: Call, response: Response) {
                responseStr = response.body?.string()
                runOnUiThread {
                    //textView1.text = responseStr
                    try {
                        val jsonRes = JSONObject(responseStr!!)
                        val jsonarr: JSONArray = jsonRes.getJSONArray("competitions")

                        for (i in 0 until jsonarr.length()) {
                            val photo_url = (jsonarr[i] as JSONObject).getJSONObject("area").optString("ensignUrl", "")
                            //var ensignUrl = "https://upload.wikimedia.org/wikipedia/commons/4/41/Flag_of_Austria.svg"
                            if((jsonarr[i] as JSONObject).optString("plan") == "TIER_ONE") {
                                if (photo_url != "null") {
                                    if (httpClient == null) { // Use cache for performance and basic offline capability
                                        httpClient = OkHttpClient.Builder()
                                            .cache(Cache(getCacheDir(), 5 * 1024 * 1014))
                                            .build()
                                    }

                                    val requestPhotoSVG: Request =
                                        Request.Builder().url(photo_url).build()
                                    httpClient!!.newCall(requestPhotoSVG)
                                        .enqueue(object : Callback {
                                            override fun onFailure(call: Call, e: java.io.IOException) {}
                                            override fun onResponse(call: Call, response: Response) {
                                                val stream: InputStream = response.body!!.byteStream()
                                                //val whatever = BitmapFactory.decodeStream(stream)
                                                val tempPic: SharpPicture =
                                                    Sharp.loadInputStream(stream)
                                                        .sharpPicture //.into(userAvatarView)
                                                runOnUiThread {
                                                    ids.add(0, (jsonarr[i] as JSONObject).optInt("id"))
                                                    countries.add(0,
                                                        (jsonarr[i] as JSONObject).getJSONObject(
                                                            "area"
                                                        ).optString("name")
                                                    )
                                                    tournaments.add(0,
                                                        (jsonarr[i] as JSONObject).optString(
                                                            "name"
                                                        )
                                                    )
                                                    photos.add(0, tempPic.drawable)
                                                    (rv_competitions.adapter as CompetitionAdapter).notifyDataSetChanged()
                                                }

                                                stream.close()
                                            }
                                        })
                                }
                                else{
                                    ids.add((jsonarr[i] as JSONObject).optInt("id"))
                                    countries.add((jsonarr[i] as JSONObject).getJSONObject("area").optString("name"))
                                    tournaments.add((jsonarr[i] as JSONObject).optString("name"))
                                    val myDrawable = resources.getDrawable(R.drawable.no_picture)
                                    photos.add(myDrawable)
                                }
                            }
                        }
                    } catch (e: Exception) {
                        //tv_name_tournament.text = "bad"
                    }
                    (rv_competitions.adapter as CompetitionAdapter).notifyDataSetChanged()
                }
            }
        })

    }

}