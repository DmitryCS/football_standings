package com.example.football_standings

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recyclerview_standings_item.view.*
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYouListener

class StandingsAdapter(val teams : ArrayList<String>, val info : ArrayList<String>, val photos : ArrayList<String>, val context: Context, val vity: Activity) : RecyclerView.Adapter<StandingsViewHolder>(){

    override fun getItemCount(): Int {
        return teams.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StandingsViewHolder {
        return StandingsViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_standings_item, parent, false))
    }

    override fun onBindViewHolder(holder: StandingsViewHolder, position: Int) {
        holder.tvTeamName.text = teams.get(position)
        holder.tvInfoTeam.text = info.get(position)
        holder.ivTeamImage.setImageDrawable(context.resources.getDrawable(R.drawable.team_icon2))
        //var url = "https://img.tsn.ua/cached/1574864746/tsn-87394d020f46df1bdfb04b5785c6f4e0/thumbs/1340x530/84/83/a32305072c1adfed28d7eed1961c8384.jpeg"
       // var url = "https://upload.wikimedia.org/wikipedia/en/4/40/Boavista_F.C._logo.svg".toUri()//photos.get(position)
//        GlideToVectorYou.justLoadImage(vity, url, holder.ivTeamImage)
        var url = photos.get(position)
        url = url.replace("https", "http")
        url = url.replace("http", "https")
        if(url != "" && url != "null") {
            var loadGlideSVG = true
            val glideSVG = GlideToVectorYou
                .init()
                .with(vity)
                .withListener(object : GlideToVectorYouListener {
                    override fun onLoadFailed() {
                        loadGlideSVG = false
                        if (url != "" && url != "null")
                            Picasso.with(context).load(url).error(context.resources.getDrawable(R.drawable.team_icon)).into(holder.ivTeamImage) //png but need access
                        //Toast.makeText(context, "Loading failed: " + url.toString(), Toast.LENGTH_SHORT).show()
                    }

                    override fun onResourceReady() {
                        //Toast.makeText(context, "Image ready", Toast.LENGTH_SHORT).show()
                    }
                })
            if(loadGlideSVG)
                glideSVG.load(url.toUri(), holder.ivTeamImage)
        }

//        if(holder.ivTeamImage.getDrawable() == null)
//            holder.ivTeamImage.setImageDrawable(context.resources.getDrawable(R.drawable.team_icon))
    }
}

class StandingsViewHolder (view: View) : RecyclerView.ViewHolder(view){
    var tvTeamName = view.tv_team_name
    var tvInfoTeam = view.tv_info_team
    var ivTeamImage= view.iv_team_image
}