package com.example.football_standings

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.recyclerview_competition_item.*
import kotlinx.android.synthetic.main.recyclerview_competition_item.view.*

class CompetitionAdapter(val ids : ArrayList<Int>, val countries : ArrayList<String>,val tournaments : ArrayList<String>, val photos : ArrayList<Drawable>, val context: Context,var mFirebaseAnalytics: FirebaseAnalytics ) : RecyclerView.Adapter<ViewHolder>(){
    override fun getItemCount(): Int {
        return countries.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_competition_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvNameCountry?.text = countries.get(position)
        holder.tvNameTournament?.text = tournaments.get(position)
        holder.ivPhotoCountry?.setImageDrawable(photos.get(position))
        holder.bStandings.setOnClickListener{
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "btn_ViewProfile")
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button_view_click")
            mFirebaseAnalytics!!.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)

            val intent = Intent(context, ActivityStandings::class.java)
            intent.putExtra("id", ids.get(position).toString())
            this.context.startActivity(intent)
        }
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    var tvNameCountry= view.tv_country
    val tvNameTournament = view.tv_name_tournament
    val ivPhotoCountry = view.iv_image
    var bStandings = view.b_standings
//    val bStandings = view.b_standings.setOnClickListener{
//        val intent = Intent(view.context, ActivityStandings::class.java)
//        //intent.putExtra("id", tvNameCountry.text.toString())
//        intent.putExtra("id", view.id)
//        view.context.startActivity(intent)
//    }
}